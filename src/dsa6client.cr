require "./client/executor"

module Client
  VERSION = "0.1.0"

  Executor.execute(ARGV)
end
