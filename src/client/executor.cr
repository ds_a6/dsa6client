require "progress"
require "socket"
require "option_parser"
require "dsa6common"

module Client
  class Executor
    include Common

    def self.execute(args)
      options = parse_args(args)

      message = build_message(options[:file])

      send_message options[:host], options[:port], message
    rescue e : Exception
      STDERR.puts "ERR: #{e.message}"
      exit -1
    end

    private def self.send_message(host : String, port : Int32, message : Messages::FileRequest)
      TCPSocket.open(host, port) do |sock|
        data = IO::Memory.new message.to_msgpack
        prog = ProgressBar.new

        prog.total = data.size
        prog.width = 40

        chunk_size = Math.log2(data.size).to_i ** 2

        loop do
          chunk = Bytes.new(chunk_size)
          count = data.read chunk

          break if count <= 0

          sock.write chunk[0, count]
          prog.tick count
        end

        prog.done unless prog.done?

        msg = Messages::Response.from_msgpack sock

        if msg.success
          puts "\nFile uploaded as '#{msg.message}'"
        else
          raise "\nFile upload failed!\n  Server response: #{msg.message}"
        end
      end
    end

    private def self.build_message(file : String)
      File.open(file) do |io|
        puts "Preparing to send #{io.size} bytes..."

        contents = Bytes.new(io.size)

        io.read contents

        filename = File.basename(file)

        Messages::FileRequest.new(filename, contents)
      end
    end

    private def self.parse_args(args)
      file = nil
      host = nil
      port = nil

      OptionParser.parse(args) do |parser|
        parser.banner = "Usage: dsa6client [args]"

        parser.on("-f FILE", "--file=FILE", "file to upload") { |_file| file = _file }
        parser.on("-h HOST", "--host=HOST", "target host to upload file to") { |_host| host = _host }
        parser.on("-p PORT", "--port=PORT", "port on target host where server is listening") { |_port| port = _port.to_i }
        parser.on("-?", "--help", "Display this help message") do
          puts parser
          exit 0
        end
      end

      raise ArgumentError.new("file not specified") if file.nil?
      raise ArgumentError.new("host not specified") if host.nil?
      raise ArgumentError.new("port not specified") if port.nil?

      {
        file: (file.as String),
        host: (host.as String),
        port: (port.as Int32),
      }
    end
  end
end
