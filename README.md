# Distributed Systems - Assignment 6 - File transfer client

## Requirements

* Crystal ~= 0.35.1

## Installation

```bash
git clone https://gitlab.com/ds_a6/dsa6client.git
cd dsa6client
shards --production build --no-debug
```

All dependencies will be resolved automatically. After building, executable will be located in `bin/` folder.

## Usage

Use `dsa6sclient --help` command to find out usage details.
